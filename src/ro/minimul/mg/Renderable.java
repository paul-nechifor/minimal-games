package ro.minimul.mg;

import javax.microedition.khronos.opengles.GL10;

public interface Renderable {
    public void draw(GL10 gl);
}
